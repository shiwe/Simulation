###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import OutputStream, LHCbApp, Gauss, GenInit

# Configure Gauss.
OutputStream('GaussTape').Output = (
    "DATAFILE='PFN:gauss.xgen' TYP='POOL_ROOTTREE' OPT='RECREATE'")
LHCbApp().EvtMax = 10
LHCbApp().DDDBtag = "dddb-20210215-8"
LHCbApp().CondDBtag = "sim-20201113-8-vc-md100-Sim10"
Gauss().Phases = ["Generator", "GenToMCTree"]
GaussGen = GenInit("GaussGen")
GaussGen.FirstEventNumber = 1
GaussGen.RunNumber = 2

# Configure the event type.
from Configurables import (Generation, Special, PowhegProduction)
from GaudiKernel import SystemOfUnits
from Gaudi.Configuration import importOptions

# Generation options.
Generation().EventType            = 42112005
Generation().PileUpTool           = "FixedLuminosityForRareProcess"
Generation().DecayTool            = ""
Generation().SampleGenerationTool = "Special"

# Special options.
Generation().addTool(Special)
Generation().Special.CutTool        = ""
Generation().Special.DecayTool      = ""
from Configurables import Gauss
sampleGenToolsOpts = { "Generator" : "Powheg" }
Gauss().SampleGenerationToolOptions.update(sampleGenToolsOpts)

# Powheg options.
sampleGenToolsOpts = {
    "Commands": ["lhans1 10770",
                 "lhans2 10770",
                 "vdecaymode 2",
                 "mass_low  40"
                ],
    "Process" : "Z"
}
Gauss().SampleGenerationToolOptions.update(sampleGenToolsOpts)
