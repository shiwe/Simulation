from Configurables import Boole
Boole().DataType  = "2018"
from Configurables import LHCbApp
LHCbApp().DDDBtag   = "dddb-20210215-8"
LHCbApp().CondDBtag = "sim-20201113-8-vc-md100-Sim10"
inputFiles = ['../Gauss/42112005_1.sim'] # wants a list of files
#inputFiles = ['Gauss-42923002-100ev-20231206.sim'] # wants a list of files
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles(inputFiles)
# name the ouput file the same as the first input file with Gauss->Boole
# DatasetName also sets histogram output file name
Boole().DatasetName = inputFiles[0].replace('Gauss','Boole').replace('.sim','')
