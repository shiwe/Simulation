# file /afs/cern.ch/user/g/gcorti/cmtuser/GaussDev_v56r5/Gen/DecFiles/options/42112005.py generated: We
#d, 13 Sep 2023 20:00:17
#
# Event Type: 42112005
#
# ASCII decay Descriptor: pp -> (Z0/gamma* -> mu+ mu-) ...
#
genAlgName="Generation"
from Configurables import Generation
Generation(genAlgName).EventType = 42112005
Generation(genAlgName).SampleGenerationTool = "Special"
from Configurables import Gauss
sampleGenToolsOpts = { "Generator" : "Powheg" }
Gauss().SampleGenerationToolOptions.update(sampleGenToolsOpts)
from Configurables import ToolSvc
from Configurables import EvtGenDecay
ToolSvc().addTool( EvtGenDecay )
ToolSvc().EvtGenDecay.UserDecayFile = "$DECFILESROOT/dkfiles/Z_mumu_PowHeg40GeV.dec"
from Configurables import Special
Generation(genAlgName).addTool( Special )
Generation(genAlgName).Special.CutTool = ""
Generation(genAlgName).FullGenEventCutTool = "LoKi::FullGenEventCut/ParsInAcc"

# Configure the event type.
from Configurables import (Generation, Special)
from GaudiKernel import SystemOfUnits
from Gaudi.Configuration import importOptions

# Generation options.
Generation(genAlgName).PileUpTool           = "FixedLuminosityForRareProcess"

# Powheg options.
from Configurables import Gauss
from GaudiKernel import SystemOfUnits

sampleGenToolsOpts = {
    "Commands": ["lhans1 10770",
                 "lhans2 10770",
                 "vdecaymode 2",
                 "mass_low  40"
                ],
    "Process" : "Z"
}
Gauss().SampleGenerationToolOptions.update(sampleGenToolsOpts)


# Generation cut
from Configurables import LoKi__FullGenEventCut
Generation(genAlgName).addTool( LoKi__FullGenEventCut, "ParsInAcc" )
tracksInAcc = Generation(genAlgName).ParsInAcc
tracksInAcc.Code = " ( ( count ( isGoodLepton ) > 0 ) ) "
tracksInAcc.Preambulo += [
     "from GaudiKernel.SystemOfUnits import  GeV, mrad"
   , "isGoodLepton   = ( ( 13 == GABSID ) & GCHARGED & ( GTHETA < 400.0*mrad ) & (GTHETA > 10.0*mrad) & ( GPT > 4.*GeV ) )"
   ]

