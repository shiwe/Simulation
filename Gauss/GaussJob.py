from Gaudi.Configuration import importOptions
importOptions('$APPCONFIGOPTS/Gauss/Beam6500GeV-md100-2018-nu1.6.py')
importOptions('$APPCONFIGOPTS/Gauss/DataType-2018.py')
importOptions('$GAUSSROOT/options/DBTags-2018.py') # overritten below
eventType = 42112005
#eventType = 42114000
importOptions('%s.py'%eventType)
importOptions('$LBPOWHEGROOT/options/PowhegPythia8.py')
importOptions("$GAUSSOPTS/GenStandAlone.py")
importOptions("$APPCONFIGOPTS/Gauss/OneFixedInteraction.py")
#importOptions('$GAUSSROOT/tests/options/testGauss-NoOutput.py')

from ProdConf import ProdConf

DETtag   = "dddb-20210215-8"
SIMtag = "sim-20201113-8-vc-md100-Sim10"

ProdConf(
  NOfEvents=10,
  DDDBTag=DETtag,
  CondDBTag=SIMtag,
  AppVersion='v55r5',
  XMLSummaryFile='summaryGauss_v55r5_%s_1.xml'%eventType,
  Application='Gauss',
  OutputFilePrefix='%s_1'%eventType,
  RunNumber=1,
  XMLFileCatalog='pool_xml_catalog.xml',
  FirstEventNumber=1,
  OutputFileTypes=['sim'],
)
